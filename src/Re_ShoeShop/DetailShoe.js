import React, { Component } from "react";

export default class DetailShoe extends Component {
  render() {
    let { name, price, quantity } = this.props.detailOfSelectedShoe;
    return (
      <div>
        <table class="table">
          <thead>
            <tr>
              <th>Name</th>
              <th>Price</th>

              <th>Stock</th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td scope="row">{name}</td>
              <td>{price}</td>

              <td>{quantity}</td>
            </tr>
          </tbody>
        </table>
      </div>
    );
  }
}
