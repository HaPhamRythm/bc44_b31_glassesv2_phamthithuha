import React, { Component } from "react";

export default class ItemShoe extends Component {
  render() {
    let { data, handleCLickToItemShoe } = this.props;
    let { image, name } = data;
    return (
      <div className="col-3 mb-3">
        <div className="card text-left h-100">
          <img className="card-img-top" src={image} alt />
          <div className="card-body">
            <h4 className="card-title">{name}</h4>
          </div>
          <button
            className="btn btn-primary"
            onClick={() => {
              handleCLickToItemShoe(data);
            }}
          >
            See More ...
          </button>
        </div>
      </div>
    );
  }
}
