import React, { Component } from "react";

export default class Footer extends Component {
  render() {
    return (
      <div>
        <div className="bg bg-dark text-white py-2">
          Copyright © Your Webite 2023
        </div>
      </div>
    );
  }
}
