import React, { Component } from "react";
import { shoeArr } from "./data";
import ListShoe from "./ListShoe";
import DetailShoe from "./DetailShoe";
import CartShoe from "./CartShoe";

export default class Ex_ShoeShop extends Component {
  state = {
    shoeArr: shoeArr,
    detailShoe: shoeArr[0],
    // cart: shoeArr,
  };
  handleViewDetail = (shoe) => {
    this.setState({
      detailShoe: shoe,
    });
  };
  // handleAddToCart = (shoe) => {
  //   let cloneCart = [...this.state.cart];
  //   let index = cloneCart.findIndex((item) => item.id == shoe.id);
  //   // th1: chưa có sp trong giỏ hàng
  //   if (index == -1) {
  //     let newShoe = { ...shoe, soLuong: 1 };
  //     cloneCart.push(newShoe);
  //   }
  //   // th2: có sp trong giỏ hàng và cập nhật số lượng
  //   else {
  //     cloneCart[index].soLuong++;
  //   }
  //   // cloneCart.push(shoe);
  //   // this.setState({ cart: cloneCart });

  //   // copy shoe và newShoe và thêm key soLuong
  // };

  // handleDelete = (idShoe) => {
  //   let cloneCart = this.state.cart.filter((item) => item.id !== idShoe);
  //   this.setState({ cart: cloneCart });
  // };

  // handleChangeAmount = (idShoe, option) => {
  //   let cloneCart = [...this.state.cart];
  //   let index = cloneCart.findIndex((item) => item.id == idShoe);
  //   cloneCart[index].soLuong = cloneCart[index].soLuong + option;
  //   //soLuong co the la +1 hoac -1
  //   if (cloneCart[index].soLuong == 0) {
  //     //sau khi update soLuong, neubang 0 thi xoa
  //     cloneCart.splice(index, 1);
  //   }
  // };

  render() {
    return (
      <div>
        <div className="row">
          {/* <CartShoe handleRemove={this.handleDelete} /> */}
          <ListShoe
            // handleBuy={this.handleAddToCart}
            list={this.state.shoeArr}
            handleViewDetail={this.handleViewDetail}
          />
        </div>

        <DetailShoe detail={this.state.detailShoe} />
      </div>
    );
  }
}
