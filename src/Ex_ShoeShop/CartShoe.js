import React, { Component } from "react";

export default class CartShoe extends Component {
  render() {
    let { cart, handleRemove } = this.props;
    return (
      <div className="col-6">
        <table class="table">
          <thead>
            <tr>
              <th>Name</th>
              <th>Amount</th>

              <th>Price</th>
              <th>Image</th>
            </tr>
          </thead>
          <tbody>
            {this.props.cart.map((item, index) => {
              return (
                <tr key={index}>
                  <td>{item.name}</td>
                  <td>
                    <button className="btn btn-warning">-</button>
                    <strong className="mx-3">-</strong>
                    <button className="btn btn-success">+</button>
                  </td>
                  <td>{item.soLuong}</td>
                  <td>{item.price * item.soLuong}</td>
                  <td>
                    <img style={{ width: 50 }} src={item.image} alt="" />
                  </td>
                  <td>
                    <button className="btn btn-danger">x</button>
                  </td>
                </tr>
              );
            })}
          </tbody>
        </table>
      </div>
    );
  }
}
