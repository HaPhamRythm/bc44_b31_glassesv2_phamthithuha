import logo from "./logo.svg";
import "./App.css";
// import DemoHeader from "./DemoComponent/DemoHeader";
// import Ex_Layout from "./Ex_Layout/Ex_Layout";
// import DataBinding from "./DataBinding/DataBinding";
// import EventHandling from "./EventHandling/EventHandling";
// import DemoState from "./DemoState/DemoState";
// import Ex_State_Car from "./Ex_State_Car/Ex_State_Car";
// import BaiTapLayoutComponent from "./BaiTapLayoutComponent/BaiTapLayoutComponent";
// import RenderWithMap from "./RenderWithMap/RenderWithMap";
// import DemoProps from "./DemoProps/DemoProps";
// import Ex_ShoeShop from "./Ex_ShoeShop/Ex_ShoeShop";
// import Re_ShoeShop from "./Re_ShoeShop/Re_ShoeShop";
import HW_Glasses from "./HW_Glasses/HW_Glasses";

function App() {
  return (
    <div className="App">
      {/* <DemoClass />
      <DemoFunction></DemoFunction> */}
      {/* <DemoHeader /> */}
      {/* <Ex_Layout /> */}
      {/* <DataBinding /> */}
      {/* <EventHandling /> */}
      {/* <DemoState /> */}
      {/* <Ex_State_Car /> */}
      {/* <BaiTapLayoutComponent /> */}
      {/* <RenderWithMap /> */}
      {/* <DemoProps /> */}
      {/* <Ex_ShoeShop /> */}
      {/* <Re_ShoeShop /> */}
      <HW_Glasses />
    </div>
  );
}

export default App;
