import React, { Component } from "react";

export default class Ex_State_Car extends Component {
  // state = {
  //   url: "./Img/CarBasic/products/black-car.jpg",
  // };

  // handleChangeColor = (color) => {
  //   this.setState({
  //     url: `./Img/CarBasic/products/${color}-car.jpg`,
  //   });
  // };

  state = {
    url: "./Img/CarBasic/products/red-car.jpg",
  };

  handleChangeColor = (color) => {
    this.setState({
      url: `./Img/CarBasic/products/${color}-car.jpg`,
    });
  };

  render() {
    return (
      <div className="row">
        {/* luon dan tu html */}
        {/* <img className="col-4" src={this.state.url} alt="" />
        <div>
          {""}

          <button
            onClick={() => {
              this.handleChangeColor("red");
            }}
            className="btn btn-danger"
          >
            Red
          </button>
          <button
            onClick={() => {
              this.handleChangeColor("black");
            }}
            className="btn btn-dark mx-5"
          >
            Black
          </button>
        </div> */}

        <img src={this.state.url} className="col-4" alt="" />
        <div>
          {"  "}
          <button
            className="btn btn-danger"
            onClick={() => {
              this.handleChangeColor("red");
            }}
          >
            Red
          </button>
          <button
            className="mx-5 btn btn-dark"
            onClick={() => {
              this.handleChangeColor("black");
            }}
          >
            Black
          </button>
        </div>
      </div>
    );
  }
}
