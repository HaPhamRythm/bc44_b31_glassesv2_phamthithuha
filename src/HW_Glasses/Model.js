import React, { Component } from "react";

export default class Model extends Component {
  urlModel = "./glasses/glassesImage/model.jpg";
  urlGlass = "./glasses/glassesImage/g9.jpg";
  render() {
    return (
      <div>
        <h3>Model</h3>
        <div style={{ position: "relative" }}>
          <img style={{ width: 200 }} src={this.urlModel} alt="f" />
          <img
            style={{
              width: 110,
              position: "absolute",
              top: "30%",
              left: "50%",
              transform: "translate(-50%,-50%)",
            }}
            src={this.props.glassModel.image}
            alt=""
          />
        </div>
      </div>
    );
  }
}
